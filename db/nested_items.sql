--This view links items to items, showing what items are inside which other items
CREATE OR REPLACE VIEW nested_items AS
    SELECT
        parent.item_name,
        parent.item_key,
        parent.item_instance,
        child.item_name       AS child_name,
        child.item_key        AS child_key,
        child.item_instance   AS child_instance,
        child.parent_key,
        child.parent_instance
    FROM
        item_instances   parent
        LEFT OUTER JOIN item_instances child ON 
            parent.item_key = child.parent_key AND parent.item_instance = child.parent_instance                                  
        WHERE child.item_key IS NOT NULL;