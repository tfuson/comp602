--Function used to calculate character level given an amount of XP

CREATE OR REPLACE FUNCTION character_level (
    xp IN NUMBER
) RETURN NUMBER IS
    char_level NUMBER;
BEGIN
    char_level := floor(log(2,(xp / 100) + 1));
    return(char_level);
END character_level;