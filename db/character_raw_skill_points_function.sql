--This function calculates the total number of skill points a character should have at a given level

CREATE OR REPLACE FUNCTION character_raw_skill_points (
    char_level IN NUMBER
) RETURN NUMBER IS
    skill_points NUMBER;
BEGIN
    skill_points := char_level * 10 + 10;
    return(skill_points);
END character_raw_skill_points;