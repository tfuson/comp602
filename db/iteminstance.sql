--This creates the iteminstance table, including constraints and populated data

DROP TABLE iteminstance;

CREATE TABLE iteminstance (
    item_key          VARCHAR2(4) NOT NULL,
    item_instance     NUMBER NOT NULL,
    item_quantity     NUMBER,
    character_key     VARCHAR2(4),
    slot_key          VARCHAR2(4),
    parent_key        VARCHAR2(4),
    parent_instance   NUMBER,
    location_key      VARCHAR(10),
    positionx                   NUMBER,
    positiony                   NUMBER,
    CONSTRAINT iteminst_pk PRIMARY KEY ( item_key, item_instance ),
    CONSTRAINT iteminst_held UNIQUE ( character_key, slot_key),
    CONSTRAINT iteminst_character_fk FOREIGN KEY ( character_key ) REFERENCES character ( character_key ),
    CONSTRAINT iteminst_slot_key_fk FOREIGN KEY ( slot_key ) REFERENCES slot ( slot_key ),
    CONSTRAINT iteminst_parent_fk FOREIGN KEY ( parent_key, parent_instance )
        REFERENCES iteminstance ( item_key,
                          item_instance ),
    CONSTRAINT item_location_key_fk FOREIGN KEY ( location_key )
        REFERENCES location ( location_key )
    ENABLE
);

--SEE IF IT IS POSSIBLE TO ENFORCE RUNE AND PACK RESTRICTIONS USING CONSTRAINTS?
--ENHANCE CONSTRAINTS TO ALLOW ONLY CHARACTER OR ITEM OR LOCATION TO BE POPULATED.

COMMENT ON COLUMN iteminstance.item_quantity IS
    'Number of items that exists in a single stack';

--Missed constraint, added after modeling
ALTER TABLE iteminstance ADD CONSTRAINT item_fk FOREIGN KEY ( item_key )
        REFERENCES item ( item_key );

-- Sample Data Load
INSERT INTO iteminstance (
    item_key,
    item_instance,
    character_key,
    slot_key
) VALUES (
    'BSWD',
    1,
    'BOBF',
    'RH'
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    character_key,
    slot_key
) VALUES (
    'BSHD',
    1,
    'BOBF',
    'LH'
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    character_key,
    slot_key
) VALUES (
    'BBP',
    1,
    'BOBF',
    'BK'
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    character_key,
    slot_key
) VALUES (
    'BARM',
    1,
    'BOBF',
    'B'
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    location_key,
    positionx,
    positiony
) VALUES (
    'BBOW',
    1,
    'NS_STORE',
    4,
    4
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    location_key,
    positionx,
    positiony
) VALUES (
    'RSWF',
    1,
    'NS_STORE',
    4,
    4
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    parent_key,
    parent_instance
) VALUES (
    'WRF',
    1,
    'RSWF',
    1
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    parent_key,
    parent_instance
) VALUES (
    'WRH',
    1,
    'RSWF',
    1
);

INSERT INTO iteminstance (
    item_key,
    item_instance,
    parent_key,
    parent_instance
) VALUES (
    'WRL',
    1,
    'RSWF',
    1
);