--Creates the character table, constraints and loads sample data
--Character's are the central component to this game

DROP TABLE character CASCADE CONSTRAINTS;

CREATE TABLE character (
    character_key    VARCHAR2(4) NOT NULL,
    character_name   VARCHAR2(40) NOT NULL,
    character_type   VARCHAR2(4) NOT NULL,
    portrait         BLOB,
    race_key         VARCHAR2(4) NOT NULL,
    xp               NUMBER,
    location_key     VARCHAR2(10),
    positionx        NUMBER DEFAULT 0,
    positiony        NUMBER DEFAULT 0,
    used_actions     NUMBER DEFAULT 0,
    used_movement    NUMBER DEFAULT 0,
    lost_health      NUMBER DEFAULT 0,
    used_endurance   NUMBER DEFAULT 0,
    lost_resolve     NUMBER DEFAULT 0,
    used_mana        NUMBER DEFAULT 0,
    strength         NUMBER DEFAULT 0,
    constitution     NUMBER DEFAULT 0,
    agility          NUMBER DEFAULT 0,
    intelligence     NUMBER DEFAULT 0,
    willpower        NUMBER DEFAULT 0,
    perception       NUMBER DEFAULT 0,
    CONSTRAINT character_pk PRIMARY KEY ( character_key ),
    CONSTRAINT char_character_type_fk FOREIGN KEY ( character_type )
        REFERENCES charactertype ( character_type_key ),
    CONSTRAINT char_race_fk FOREIGN KEY ( race_key )
        REFERENCES race ( race_key ),
    CONSTRAINT char_location_fk FOREIGN KEY ( location_key )
        REFERENCES location ( location_key )
    ENABLE
);

COMMENT ON COLUMN character.character_type IS
    'Type of character, example playable vs. non-playable';

COMMENT ON COLUMN character.xp IS
    'Experience';

COMMENT ON COLUMN character.used_actions IS
    'Used Action Points';

COMMENT ON COLUMN character.used_movement IS
    'Used Movement Points';

--Sample data load

INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'BOBF',
    'Bob the Fighter',
    'PC',
    'ORC',
    0,
    'WORLD',
    16,
    20,
    16,
    10,
    10,
    8,
    8,
    8
);

INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'ROGR',
    'Roger Rogue',
    'PC',
    'HLFL',
    0,
    'WORLD',
    16,
    20,
    8,
    8,
    16,
    8,
    8,
    12
);


INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'NANC',
    'Nancy Cleric',
    'PC',
    'HUMN',
    0,
    'WORLD',
    16,
    20,
    12,
    8,
    8,
    8,
    16,
    8
);


INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'KIMA',
    'Kimmi the Archer',
    'PC',
    'ELF',
    0,
    'WORLD',
    16,
    20,
    8,
    8,
    12,
    8,
    8,
    16
);


INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'VICV',
    'Victor Vendor',
    'NPC',
    'HUMN',
    0,
    'NS_STORE',
    4,
    4,
    10,
    10,
    10,
    10,
    10,
    10
);


INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'BIGB',
    'Big Badguy',
    'NPC',
    'OGRE',
    0,
    'DESERTO',
    8,
    8,
    10,
    10,
    10,
    10,
    10,
    10
);

INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'UNLR',
    'Unlucky Rogue',
    'NPC',
    'GNM',
    0,
    'DESERTO',
    7,
    8,
    10,
    10,
    10,
    10,
    10,
    10
);

INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'UNLW',
    'Unlucky Wizard',
    'NPC',
    'ELF',
    0,
    'DESERTO',
    9,
    8,
    10,
    10,
    10,
    10,
    10,
    10
);

INSERT INTO character (
    character_key,
    character_name,
    character_type,
    race_key,
    xp,
    location_key,
    positionx,
    positiony,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception
) VALUES (
    'UNLC',
    'Unlucky Cleric',
    'NPC',
    'HUMN',
    0,
    'DESERTO',
    9,
    9,
    10,
    10,
    10,
    10,
    10,
    10
);