--This creates the location table, constraints and sample data
--This is the places where characters and items can reside.

DROP TABLE location;

CREATE TABLE location (
    location_key         VARCHAR2(10) NOT NULL,
    location_name        VARCHAR2(40) NOT NULL,
    location_desc        VARCHAR2(4000),
    location_parent      VARCHAR2(10),
    location_image       BLOB,
    location_terrain     BLOB,
    location_sizex       NUMBER,
    location_sizey       NUMBER,    
    location_positionx   NUMBER,
    location_positiony   NUMBER,
    CONSTRAINT location_pk PRIMARY KEY ( location_key ),
    CONSTRAINT location_parent_fk FOREIGN KEY ( location_parent ) REFERENCES location (location_key)
    ENABLE
);

--sample data
INSERT INTO location (
    location_key,
    location_name,
    location_desc,
    location_sizex,
    location_sizey
) VALUES (
    'WORLD',
    'World Map',
    'The world of stabbing mountains',
    64,
    64
);

INSERT INTO location (
    location_key,
    location_name,
    location_desc,
    location_parent,
    location_sizex,
    location_sizey,
    location_positionx,
    location_positiony
) VALUES (
    'CITYOFBRID',
    'City of Bridges',
    'The center of commerce',
    'WORLD',
    32,
    32,
    32,
    32
);

INSERT INTO location (
    location_key,
    location_name,
    location_desc,
    location_parent,
    location_sizex,
    location_sizey,
    location_positionx,
    location_positiony
) VALUES (
    'NORTHSTEAD',
    'Northstead',
    'Small town in the north',
    'WORLD',
    16,
    16,
    20,
    60
);

INSERT INTO location (
    location_key,
    location_name,
    location_desc,
    location_parent,
    location_sizex,
    location_sizey,
    location_positionx,
    location_positiony
) VALUES (
    'NS_STORE',
    'Northstead Store',
    'Store in the town of Northstead',
    'NORTHSTEAD',
    8,
    8,
    8,
    8
);

INSERT INTO location (
    location_key,
    location_name,
    location_desc,
    location_parent,
    location_sizex,
    location_sizey,
    location_positionx,
    location_positiony
) VALUES (
    'DESERTO',
    'Desert Oasis',
    'Small Oasis in the middle of the desert',
    'WORLD',
    16,
    16,
    40,
    10
);