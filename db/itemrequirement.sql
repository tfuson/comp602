--Creates the itemrequirement table, constraints and loads sample data
--This shows all of the requirements associated with items.

DROP TABLE itemrequirement;

CREATE TABLE itemrequirement (
    item_key                 VARCHAR2(4) NOT NULL,
    item_equip_requirement   VARCHAR2(400) NOT NULL,
    CONSTRAINT itemreq_item_fk FOREIGN KEY ( item_key )
        REFERENCES item ( item_key )
    ENABLE
);

COMMENT ON COLUMN itemrequirement.item_equip_requirement IS
    'Requirement to equip an item in a particular slot';

--Future enchancement: Update requirements to support SQL

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'BSWD',
    'SLOT IN (LH, RH)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'ISWD',
    'SLOT IN (LH, RH)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'ASWD',
    'SLOT IN (LH, RH)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'RSWF',
    'SLOT IN (LH, RH)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'RSWF',
    'CHARACTER.LEVEL > 4'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'WRF',
    'ITEMRUNECOUNT < SELECT COUNT RUNES ASSOCIATED WITH ITEM'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'WRF',
    'ITEMBENEFIT IN (RUNIC)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'WRH',
    'ITEMRUNECOUNT < SELECT COUNT RUNES ASSOCIATED WITH ITEM'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'WRH',
    'ITEMBENEFIT IN (RUNIC)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'WRL',
    'ITEMRUNECOUNT < SELECT COUNT RUNES ASSOCIATED WITH ITEM'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'WRL',
    'ITEMBENEFIT IN (RUNIC)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'BSHD',
    'SLOT IN (LH, RH)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'BSHD',
    'SELECT COUNT(*) from CHARACTER.EQUIPMENT where ITEM_TYPE = SHLD < 1'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'BBOW',
    'SLOT IN (LH, RH)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'BARM',
    'SLOT IN (B)'
);

INSERT INTO itemrequirement (
    item_key,
    item_equip_requirement
) VALUES (
    'BBP',
    'SLOT IN (BK)'
);