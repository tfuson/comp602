--This creates the skillbenefit table, constraints and sample data
--This table associates specific benefits with a skill

DROP TABLE skillbenefit;

CREATE TABLE skillbenefit (
    skill_key       VARCHAR2(4) NOT NULL,
    skilltree_key   VARCHAR2(4) NOT NULL,
    skillbenefit    VARCHAR2(400) NOT NULL,
    CONSTRAINT skillben_pk PRIMARY KEY (skill_key, skilltree_key),
    CONSTRAINT skillben_skill_fk FOREIGN KEY ( skill_key,
                                               skilltree_key )
        REFERENCES skill ( skill_key,
                               skilltree_key )
    ENABLE
);

--Future Enchancement: Better support for SQL queries, link to the keyword table maybe?
--Linking to keyword could present challenges for skills which have complex ingame effects, like Charge for example

--sample data
INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'SWNG',
    'OHW',
    '+10 Attack with Melee'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'CHRG',
    'OHW',
    '+10 Movement followed by Attack'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'CHRG',
    'OHW',
    '+10 Attack with Melee'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'SWNG',
    'THW',
    '+10 Damage with Melee'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'WEAK',
    'RACE',
    '-2 Strength'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'STRG',
    'RACE',
    '+2 Strength'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'CLMS',
    'RACE',
    '-2 Agility'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'AGIL',
    'RACE',
    '+2 Agility'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'STOU',
    'RACE',
    '+2 Constitution'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'DUMB',
    'RACE',
    '-2 Intelligence'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'SMRT',
    'RACE',
    '+2 Intelligence'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'ADPT',
    'RACE',
    '+10 Skill Points'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'PERS',
    'RACE',
    '+2 Perception'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'FLGT',
    'RACE',
    'Ignore surface terrain effects'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'DIVA',
    'RACE',
    '-2 To all attacks for all opponents within 10 squares'
);

INSERT
    INTO skillbenefit (
    skill_key,
    skilltree_key,
    skillbenefit
) VALUES (
    'POSS',
    'RACE',
    'Air Attack, on hit govern character actions for the next 2 turns'
);