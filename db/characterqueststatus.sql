--This creates the characterqueststatus table, constraints and sample data
--This shows the current status of each quest

DROP TABLE characterqueststatus;

CREATE TABLE characterqueststatus (
    quest_status VARCHAR(10) NOT NULL,
    CONSTRAINT quest_status_pk PRIMARY KEY (quest_status)
    ENABLE
);

--Load sample data
INSERT INTO characterqueststatus (
    quest_status
) VALUES (
    'ACTIVE'
);

INSERT INTO characterqueststatus (
    quest_status
) VALUES (
    'COMPLETED'
);

INSERT INTO characterqueststatus (
    quest_status
) VALUES (
    'FAILED'
);

INSERT INTO characterqueststatus (
    quest_status
) VALUES (
    'IGNORED'
);