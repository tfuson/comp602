--This creates the skilltree table, constraints and sample data
--This allows skills to be grouped into related categories
DROP TABLE skilltree;

CREATE TABLE skilltree (
    skilltree_key    VARCHAR2(4) NOT NULL,
    skilltree_name   VARCHAR2(40) NOT NULL,
    skilltree_desc   VARCHAR2(400) NOT NULL,
    CONSTRAINT skilltree_pk PRIMARY KEY ( skilltree_key ) ENABLE
);

--sample data
INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'OHW',
    'One Handed Weapons',
    'For fighters wanting to fight one handed or with a shield'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'THW',
    'Two Handed Weapons',
    'For those looking to sacrifice defence for offence'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'FIRE',
    'Fire Magic',
    'Magic users looking to deal a great amount of damage with the fire elements'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'AIR',
    'Air Magic',
    'Magic users looking to augment the capabilities of allies and penalize opponents'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'WATR',
    'Water Magic',
    'Magic users looking to heal and protect'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'EART',
    'Earth Magic',
    'Magic users wishing to control the battlefield'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'RANG',
    'Ranged Weapons',
    'For those that wish to attack at range with bows or crossbows'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'THF',
    'Two Handed Fighting',
    'For those looking to weild two weapons simultaneously'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'SHLD',
    'Shield Use',
    'For those looking effectively use shields'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'RACE',
    'Racial Abilities',
    'Innate skills associated with specific races'
);

INSERT INTO skilltree (
    skilltree_key,
    skilltree_name,
    skilltree_desc
) VALUES (
    'ITEM',
    'Item Special Abilities',
    'Abilities associated with items'
);