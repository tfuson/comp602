--This creates the charactercondition table, contraints and loads sample data
--The character condition is what the current state the character could be in, from DEAD to no conditions at all.

DROP TABLE charactercondition;

CREATE TABLE charactercondition (
    character_key   VARCHAR2(4) NOT NULL,
    condition_key   VARCHAR2(4) NOT NULL,
    CONSTRAINT charcond_pk PRIMARY KEY ( character_key,
                                         condition_key ),
    CONSTRAINT charcond_character_fk FOREIGN KEY ( character_key )
        REFERENCES character ( character_key ),
    CONSTRAINT charcond_condition_fk FOREIGN KEY ( condition_key )
        REFERENCES condition ( condition_key )
    ENABLE
);

--Sample data load
INSERT INTO charactercondition (
    character_key,
    condition_key
) VALUES (
    'UNLR',
    'PT'
);

INSERT INTO charactercondition (
    character_key,
    condition_key
) VALUES (
    'UNLR',
    'ROOT'
);

INSERT INTO charactercondition (
    character_key,
    condition_key
) VALUES (
    'UNLW',
    'PT'
);

INSERT INTO charactercondition (
    character_key,
    condition_key
) VALUES (
    'UNLW',
    'FLEE'
);

INSERT INTO charactercondition (
    character_key,
    condition_key
) VALUES (
    'UNLC',
    'DEAD'
);