--This view pivots the components into a comma seperated list of subcomponents
CREATE VIEW item_components AS
    SELECT
        item_key,
        item_instance,
        item_name,
        LISTAGG(child_name, ', ') WITHIN GROUP(
            ORDER BY
                child_name
        ) components
    FROM
        nested_items
    GROUP BY
        item_name,
        item_key,
        item_instance;