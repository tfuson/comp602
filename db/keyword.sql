-- The keyword table replaces the damagetype table in order to provide a potential means of using SQL queries to SUM() all damage bonuses associated with bonuses provided by skills and items.
-- This script converts the damagetype table into the keyword table.

-- Create, delete and modify a table, including all its fields.
-- Demonstration of the conversion of the damagetype table to a more generic keyword and keywordgroup table structure.
-- Normally it would be superior to just create the new table structure and migrate the data, but this gives an opportunity to demonstrate learning outcomes.
DROP TABLE keyword;

CREATE TABLE keyword
    AS
        SELECT
            *
        FROM
            damagetype;

ALTER TABLE keyword RENAME COLUMN damage_type_key TO keyword_key;

ALTER TABLE keyword RENAME COLUMN damage_type_name TO keyword_name;

ALTER TABLE keyword RENAME COLUMN damage_type_desc TO keyword_desc;

ALTER TABLE keyword MODIFY (
    keyword_desc NULL
);

--Add support for types of keywords, starting with DAMAGE

ALTER TABLE keyword 
ADD (keyword_type_key VARCHAR2(4));

UPDATE keyword
SET keyword_type_key = 'DMG'; --No where clause to update all rows.

ALTER TABLE keyword MODIFY (
    keyword_type_key NOT NULL);

ALTER TABLE keyword ADD CONSTRAINT keyword_pk PRIMARY KEY ( keyword_key );

DROP table keywordtype;

CREATE TABLE keywordtype (
    keyword_type_key         VARCHAR2(4) NOT NULL,
    keyword_type_name        VARCHAR2(40) NOT NULL,
    keyword_type_desc        VARCHAR2(400) NOT NULL,
    CONSTRAINT keywordtype_pk PRIMARY KEY ( keyword_type_key ) ENABLE
);

INSERT INTO keywordtype (
    keyword_type_key,
    keyword_type_name,
    keyword_type_desc
) VALUES (
    'DMG',
    'Damage',
    'Types of Damage'
);

ALTER TABLE keyword
    ADD CONSTRAINT keyword_type_fk FOREIGN KEY ( keyword_type_key )
        REFERENCES keywordtype ( keyword_type_key );

SELECT
    *
FROM
    all_cons_columns
WHERE
    column_name = 'DAMAGE_TYPE_KEY';
--FOUND RACERESISTANCE, RACEBONUS. Updating those specific scripts.

ALTER TABLE raceresistance DROP CONSTRAINT raceresistance_damage_type_fk;

ALTER TABLE raceresistance
    ADD CONSTRAINT raceresistance_keyword_fk FOREIGN KEY ( damage_type_key )
        REFERENCES keyword ( keyword_key );

ALTER TABLE racebonus DROP CONSTRAINT RACEBONUS_DAMAGE_TYPE_FK;

ALTER TABLE racebonus
    ADD CONSTRAINT racebonus_keyword_fk FOREIGN KEY ( damage_type_key )
        REFERENCES keyword ( keyword_key );