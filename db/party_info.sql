--Some high level information about each party, the number of characters in each and the highest level person in the party.
--Future enchancements could be made to SUM the levels of characters and do some other calculations on these numbers to find the relative party strength to create balanced encounters.
CREATE OR REPLACE VIEW party_info AS
    SELECT
        party_name,
        COUNT(character.character_key) AS number_of_characters,
        MAX(character_level(character.xp)) AS highest_level_character
    FROM
        character
        JOIN partycharacters ON character.character_key = partycharacters.character_key
        JOIN party ON partycharacters.party_key = party.party_key
    GROUP BY
        party.party_name;