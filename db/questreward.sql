--This creates the questreward table, constraints and sample data
--This is what a character receives when they complete a quest.
DROP TABLE questreward;

CREATE TABLE questreward (
    quest_key           VARCHAR2(10) NOT NULL,
    quest_questreward   VARCHAR2(400) NOT NULL,
    CONSTRAINT questreward_quest_key_fk FOREIGN KEY ( quest_key )
        REFERENCES quest ( quest_key )
    ENABLE
);

-- Future Enchancement: Update requirements to support SQL queries, support random item generation through functions and SQL

--Sample Data
INSERT INTO questreward (
    quest_key,
    quest_questreward
) VALUES (
    'FQ1',
    'Item=GP, QTY=50'
);

INSERT INTO questreward (
    quest_key,
    quest_questreward
) VALUES (
    'SQ1',
    'XP+50'
);

INSERT INTO questreward (
    quest_key,
    quest_questreward
) VALUES (
    'TQ1',
    'Item=SpecificItem'
);

INSERT INTO questreward (
    quest_key,
    quest_questreward
) VALUES (
    'TQ1',
    'XP+50'
);

INSERT INTO questreward (
    quest_key,
    quest_questreward
) VALUES (
    'HQ1',
    'XP+500'
);

INSERT INTO questreward (
    quest_key,
    quest_questreward
) VALUES (
    'HQ2',
    'XP+1000'
);

INSERT INTO questreward (
    quest_key,
    quest_questreward
) VALUES (
    'HQ2',
    'Item=RandomItem'
);