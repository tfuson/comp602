--This creates the questoffer table, constraints and sample data
--This is what state a character needs to be at in order for a quest to be offered to the character.
--Without this the character would get all possible quests offered immediately, and it wouldn't be possible to craft a story of linked quests.

DROP TABLE questoffer;

CREATE TABLE questoffer (
    quest_key                 VARCHAR2(10) NOT NULL,
    quest_offer_requirement   VARCHAR2(4000) NOT NULL
);

ALTER TABLE questoffer
    ADD CONSTRAINT questoffer_quest_key_fk FOREIGN KEY ( quest_key )
        REFERENCES quest ( quest_key );

-- Future Enchancement: Update requirements to better support SQL queries

--Sample Data
INSERT INTO questoffer (
    quest_key,
    quest_offer_requirement
) VALUES (
    'FQ1',
    'No Active Quests'
);

INSERT INTO questoffer (
    quest_key,
    quest_offer_requirement
) VALUES (
    'FQ1',
    'No Completed Quests'
);

INSERT INTO questoffer (
    quest_key,
    quest_offer_requirement
) VALUES (
    'SQ1',
    'Completed First Quest'
);

INSERT INTO questoffer (
    quest_key,
    quest_offer_requirement
) VALUES (
    'TQ1',
    'Level > 1'
);

INSERT INTO questoffer (
    quest_key,
    quest_offer_requirement
) VALUES (
    'HQ1',
    'Level > 2'
);

INSERT INTO questoffer (
    quest_key,
    quest_offer_requirement
) VALUES (
    'HQ2',
    'Level > 3'
);
