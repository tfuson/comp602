--This creates the damagetype table, constraints and sample data
--This stores the damage types in the game.

--DEPRICATED, see keyword and keywordtype tables instead

DROP TABLE damagetype;

CREATE TABLE damagetype (
    damage_type_key         VARCHAR2(4) NOT NULL,
    damage_type_name        VARCHAR2(40) NOT NULL,
    damage_type_desc        VARCHAR2(400) NOT NULL,
    CONSTRAINT damagetype_pk PRIMARY KEY ( damage_type_key ) ENABLE
);

COMMENT ON TABLE damagetype
   IS 'Damage Types. Among all of the damage types an attack has the lowest resistance score is used against that damage. Weapons and attacks may have multiple damage types.';

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'CUT',
    'Cutting',
    'Damage done by physical cutting and slashing attacks'
);

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'CRSH',
    'Crushing',
    'Damage done by physical crushing force and impact'
);

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'PUNC',
    'Puncture',
    'Damage done by physical puncture and piercing attacks'
);

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'POIS',
    'Poison',
    'Damage done by physical poison, venom, acid and other chemical attacks'
);

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'AIR',
    'Air',
    'Damage done by magic means relating to the air element'
);

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'WATR',
    'Water',
    'Damage done by magic means relating to the water element'
);

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'ERTH',
    'Earth',
    'Damage done by magic means relating to the earth element'
);

INSERT INTO damagetype (
    damage_type_key,
    damage_type_name,
    damage_type_desc
) VALUES (
    'FIRE',
    'Fire',
    'Damage done by magic means relating to the fire element'
);