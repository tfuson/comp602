--This creates the skill table, constraints and sample data
--The skill table stores skills that could be attained by a character
DROP TABLE skill cascade constraints;

CREATE TABLE skill (
    skill_key       VARCHAR2(4) NOT NULL,
    skill_name      VARCHAR2(40) NOT NULL,
    skill_desc      VARCHAR2(400) NOT NULL,
    skilltree_key   VARCHAR2(4) NOT NULL,
    skill_cost      NUMBER,
    action_cost     NUMBER,
    CONSTRAINT skill_pk PRIMARY KEY ( skill_key,
                                      skilltree_key ),
    CONSTRAINT skill_skilltree_fk FOREIGN KEY ( skilltree_key )
        REFERENCES skilltree ( skilltree_key )
    ENABLE
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'SWNG',
    'Swing',
    'Basic Training with one Handed Melee Weapons',
    'OHW',
    10,
    6
);

INSERT INTO skill ( skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'STRK',
    'Strike',
    'Basic Combat techniques with Melee Weapons',
    'OHW',
    20,
    6
);

INSERT INTO skill ( skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'CHRG',
    'Charge',
    'Charge in and make a Melee Attack',
    'OHW',
    30,
    10
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'SWNG',
    'Swing',
    'Basic Training with Two Handed Melee Weapons',
    'THW',
    10,
    8
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'BLK',
    'Block',
    'Basic Training with Shields',
    'SHLD',
    10,
    4
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'WEAK',
    'Weak',
    'This race is not known for physical strength',
    'RACE',
    -10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'STRG',
    'Strong',
    'This race is known for physical strength',
    'RACE',
    10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'CLMS',
    'Clumsy',
    'This race is not known for being agile',
    'RACE',
    -10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'AGIL',
    'Agile',
    'This race is known for being agile',
    'RACE',
    -10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'STOU',
    'Stout',
    'This race is known for having a strong consititution',
    'RACE',
    10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'DUMB',
    'Dumb',
    'This race is not known for being smart',
    'RACE',
    -10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'SMRT',
    'Smart',
    'This race is known for being smart',
    'RACE',
    10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'ADPT',
    'Adaptable',
    'This race is known for being adaptable',
    'RACE',
    10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'PERS',
    'Perceptive',
    'This race is known for being perceptive',
    'RACE',
    10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'FLGT',
    'Flight',
    'This race can fly',
    'RACE',
    10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'DIVA',
    'Divine Aura',
    'This race has a divine aura',
    'RACE',
    10,
    0
);

INSERT INTO skill (
    skill_key,
    skill_name,
    skill_desc,
    skilltree_key,
    skill_cost,
    action_cost
) VALUES (
    'POSS',
    'Possession',
    'This race is able to possess characters',
    'RACE',
    10,
    0
);