--This view shows all of the specific instances of items, flattened to include full item names and types information
CREATE VIEW item_instances AS
SELECT
    item.item_key,
    item_instance,
    item_quantity,
    character_key,
    slot_key,
    parent_key,
    parent_instance,
    location_key,
    positionx,
    positiony,
    item_name,
    item_desc,
    item.item_type_key,
    item_picture,
    item_type_name,
    item_type_desc,
    item_max_stack_size
    FROM
        iteminstance
        JOIN item ON iteminstance.item_key = item.item_key
        JOIN itemtype ON item.item_type_key = itemtype.item_type_key;
        
