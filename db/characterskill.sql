--This creates the characterskill table, constraints and sample data
--This stores what skills each character has

DROP TABLE characterskill;

CREATE TABLE characterskill (
    character_key   VARCHAR2(4) NOT NULL,
    skill_key       VARCHAR2(4) NOT NULL,
    skilltree_key   VARCHAR2(4) NOT NULL,
    CONSTRAINT charskill_pk PRIMARY KEY ( character_key,
                                          skill_key, skilltree_key ),
    CONSTRAINT charskill_character_key_fk FOREIGN KEY ( character_key )
        REFERENCES character ( character_key ),
    CONSTRAINT charskill_skill_key_fk FOREIGN KEY ( skill_key, skilltree_key)
        REFERENCES skill ( skill_key, skilltree_key )
    ENABLE
);

--Sample data load
INSERT INTO characterskill (
    character_key,
    skill_key,
    skilltree_key
) VALUES (
    'BOBF',
    'SWNG',
    'OHW'
);

INSERT INTO characterskill (
    character_key,
    skill_key,
    skilltree_key
) VALUES (
    'UNLC',
    'SWNG',
    'OHW'
);

INSERT INTO characterskill (
    character_key,
    skill_key,
    skilltree_key
) VALUES (
    'UNLC',
    'STRK',
    'OHW'
);

INSERT INTO characterskill (
    character_key,
    skill_key,
    skilltree_key
) VALUES (
    'UNLC',
    'CHRG',
    'OHW'
);

INSERT INTO characterskill (
    character_key,
    skill_key,
    skilltree_key
) VALUES (
    'UNLC',
    'BLK',
    'SHLD'
);

INSERT INTO characterskill (
    character_key,
    skill_key,
    skilltree_key
) VALUES (
    'UNLR',
    'SWNG',
    'THW'
);