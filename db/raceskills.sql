--This creates the raceskills table, constraints and sample data
--This table associates specific special racial skills to each race. This can have important ingame effects like ability score increases, flying, darkvision and so on.

DROP TABLE raceskills;

CREATE TABLE raceskills (
    race_key        VARCHAR2(4) NOT NULL,
    skill_key       VARCHAR2(4) NOT NULL,
    skilltree_key   VARCHAR2(4) NOT NULL,
    CONSTRAINT raceskills_pk PRIMARY KEY ( race_key,
                                           skill_key,
                                           skilltree_key ),
    CONSTRAINT raceskills_race_key_fk FOREIGN KEY ( race_key )
        REFERENCES race ( race_key ),
    CONSTRAINT raceskills_skill_key_fk FOREIGN KEY ( skill_key,skilltree_key )
        REFERENCES skill ( skill_key,skilltree_key )
    ENABLE   
);

--sample data
INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'HUMN',
    'ADPT',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'ELF',
    'SMRT',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'ELF',
    'PERS',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'DWRF',
    'STRG',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'DWRF',
    'STOU',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'ORC',
    'DUMB',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'ORC',
    'STRG',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'HLFL',
    'AGIL',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'HLFL',
    'PERS',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'OGRE',
    'STRG',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'OGRE',
    'DUMB',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'GOBL',
    'AGIL',
    'RACE'
);


INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'GOBL',
    'WEAK',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'GNM',
    'SMRT',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'GNM',
    'AGIL',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'GNM',
    'WEAK',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'FRSP',
    'FLGT',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'CELS',
    'FLGT',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'CELS',
    'DIVA',
    'RACE'
);

INSERT INTO raceskills (
    race_key,
    skill_key,
    skilltree_key
) VALUES (
    'DEMN',
    'POSS',
    'RACE'
);

