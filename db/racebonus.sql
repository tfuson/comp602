--Creates the racebonus table, constraints and loads sample data
--This associates certain benefits to a character depending on the race they have selected for thier character.
DROP TABLE racebonus;

CREATE TABLE racebonus (
    race_key          VARCHAR2(4) NOT NULL,
    damage_type_key   VARCHAR2(4) NOT NULL,
    bonus        NUMBER,
    CONSTRAINT racebonus_pk PRIMARY KEY ( race_key,
                                               damage_type_key ),
    CONSTRAINT racebonus_race_key_fk FOREIGN KEY ( race_key )
        REFERENCES race ( race_key ),
    CONSTRAINT racebonus_damage_type_fk FOREIGN KEY ( damage_type_key )
        REFERENCES damagetype ( damage_type_key )
    ENABLE
);

--Conversion of damage_type to keyword table
ALTER TABLE racebonus DROP CONSTRAINT RACEBONUS_DAMAGE_TYPE_FK;

ALTER TABLE racebonus
    ADD CONSTRAINT racebonus_keyword_fk FOREIGN KEY ( damage_type_key )
        REFERENCES keyword ( keyword_key );

COMMENT on COLUMN racebonus.bonus is
    'Damage bonus to attack damage in percentage';

--sample data
INSERT INTO racebonus (
    race_key,
    damage_type_key,
    bonus
) VALUES (
    'FRSP',
    'FIRE',
    100
);

INSERT INTO racebonus (
    race_key,
    damage_type_key,
    bonus
) VALUES (
    'OGRE',
    'CRSH',
    100
);

INSERT INTO racebonus (
    race_key,
    damage_type_key,
    bonus
) VALUES (
    'ORC',
    'CUT',
    20
);

INSERT INTO racebonus (
    race_key,
    damage_type_key,
    bonus
) VALUES (
    'ORC',
    'CRSH',
    20
);

INSERT INTO racebonus (
    race_key,
    damage_type_key,
    bonus
) VALUES (
    'ORC',
    'PUNC',
    20
);

INSERT INTO racebonus (
    race_key,
    damage_type_key,
    bonus
) VALUES (
    'GOBL',
    'POIS',
    20
);

