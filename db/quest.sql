--This creates the quest table, constraints and sample data
--Quests are things that characters can do in order to get rewards of items and xp.

DROP TABLE quest CASCADE CONSTRAINTS;

CREATE TABLE quest (
    quest_key    VARCHAR2(10) NOT NULL,
    quest_name   VARCHAR2(40) NOT NULL,
    quest_desc   VARCHAR2(4000),
    CONSTRAINT quest_pk PRIMARY KEY ( quest_key ) ENABLE
);

COMMENT ON COLUMN quest.quest_key IS
    'Quest Short Key';

COMMENT ON COLUMN quest.quest_name IS
    'Quest name used in the game';

COMMENT ON COLUMN quest.quest_desc IS
    'Quest description used in the game';

--sample data
INSERT INTO quest (
    quest_key,
    quest_name,
    quest_desc
) VALUES (
    'FQ1',
    'First Quest',
    'Welcome to your first quest, to complete this equip your starting equipment'
);

INSERT INTO quest (
    quest_key,
    quest_name,
    quest_desc
) VALUES (
    'SQ1',
    'Second Quest',
    'Now it is time to test your combat skill, attack the combat dummy'
);

INSERT INTO quest (
    quest_key,
    quest_name,
    quest_desc
) VALUES (
    'TQ1',
    'Third Quest',
    'Talk to the storekeeper in Northstead'
);

INSERT INTO quest (
    quest_key,
    quest_name,
    quest_desc
) VALUES (
    'HQ1',
    'Hunter''s Quest',
    'Find and kill 3 wolves'
);

INSERT INTO quest (
    quest_key,
    quest_name,
    quest_desc
) VALUES (
    'HQ2',
    'Hunter''s Quest',
    'Hunt down 4 goblins'
);
