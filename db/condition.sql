--This creates the condition table, constraints and sample data
--This stores specific details on each condition that could be assigned to a character

DROP TABLE condition cascade constraints;

CREATE TABLE condition (
    condition_key    VARCHAR2(4) NOT NULL,
    condition_name   VARCHAR2(40) NOT NULL,
    condition_desc   VARCHAR2(400) NOT NULL,
    CONSTRAINT condition_pk PRIMARY KEY ( condition_key ) ENABLE
);

--Sample data
INSERT INTO condition (
    condition_key,
    condition_name,
    condition_desc
) VALUES (
    'FLEE',
    'Fleeing',
    'Character Resolve has reached 0'
);

INSERT INTO condition (
    condition_key,
    condition_name,
    condition_desc
) VALUES (
    'DEAD',
    'Dead',
    'Character Health has reached Negative Max Health or some other effect has caused Death'
);

INSERT INTO condition (
    condition_key,
    condition_name,
    condition_desc
) VALUES (
    'U',
    'Unconcious',
    'Character Health is 0 or lower, but higher than Negative Max Health'
);

INSERT INTO condition (
    condition_key,
    condition_name,
    condition_desc
) VALUES (
    'PT',
    'Petrified',
    'Character is unable to move or take actions'
);

INSERT INTO condition (
    condition_key,
    condition_name,
    condition_desc
) VALUES (
    'FZ',
    'Frozen',
    'Character is encased in ice and is unable to move or take actions with the exception of breaking free'
);

INSERT INTO condition (
    condition_key,
    condition_name,
    condition_desc
) VALUES (
    'ROOT',
    'Rooted',
    'Character is unable to move'
);