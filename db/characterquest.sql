--This creates the characterquest table, constraints and sample data
--This table shows what quests each character has

DROP TABLE characterquest;

CREATE TABLE characterquest (
    character_key   VARCHAR2(4) NOT NULL,
    quest_key       VARCHAR2(4) NOT NULL,
    quest_status    VARCHAR2(10) NOT NULL,
    CONSTRAINT charquest_pk PRIMARY KEY (character_key, quest_key),
    CONSTRAINT charquest_character_key_fk FOREIGN KEY ( character_key )
        REFERENCES character ( character_key ),
    CONSTRAINT charquest_quest_key_fk FOREIGN KEY ( quest_key )
        REFERENCES quest ( quest_key ),
    CONSTRAINT charquest_quest_status_fk FOREIGN KEY ( quest_status )
        REFERENCES characterqueststatus ( quest_status )
    ENABLE       
);

--Load of sample data
INSERT INTO characterquest (
    character_key,
    quest_key,
    quest_status
) VALUES (
    'BOBF',
    'FQ1',
    'ACTIVE'
);

INSERT INTO characterquest (
    character_key,
    quest_key,
    quest_status
) VALUES (
    'ROGR',
    'FQ1',
    'IGNORED'
);

INSERT INTO characterquest (
    character_key,
    quest_key,
    quest_status
) VALUES (
    'NANC',
    'FQ1',
    'IGNORED'
);

INSERT INTO characterquest (
    character_key,
    quest_key,
    quest_status
) VALUES (
    'KIMA',
    'FQ1',
    'IGNORED'
);

INSERT INTO characterquest (
    character_key,
    quest_key,
    quest_status
) VALUES (
    'UNLR',
    'FQ1',
    'COMPLETED'
);

INSERT INTO characterquest (
    character_key,
    quest_key,
    quest_status
) VALUES (
    'UNLR',
    'SQ1',
    'COMPLETED'
);

INSERT INTO characterquest (
    character_key,
    quest_key,
    quest_status
) VALUES (
    'UNLR',
    'TQ1',
    'FAILED'
);