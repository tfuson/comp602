--This creates the charactertype table, constraints and sample data
--This stores the character type, whether the character is player controlled or computer controlled.

DROP TABLE charactertype CASCADE CONSTRAINTS;

CREATE TABLE charactertype (
    character_type_key    VARCHAR2(4) NOT NULL,
    character_type_name   VARCHAR2(40) NOT NULL,
    character_type_desc   VARCHAR2(400),
    CONSTRAINT character_type_pk PRIMARY KEY ( character_type_key ) ENABLE
);

INSERT INTO charactertype (
    character_type_key,
    character_type_name
) VALUES (
    'PC',
    'Player Character'
);

INSERT INTO charactertype (
    character_type_key,
    character_type_name
) VALUES (
    'NPC',
    'Non-Player Character'
);