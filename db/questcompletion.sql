--This creates the questcompletion table, constraints and sample data
--This table defines how a quest will be determined if it has been successful or not.
--There can be many steps or requirements for each quest.

DROP TABLE questcompletion;

CREATE TABLE questcompletion (
    quest_key                      VARCHAR2(10) NOT NULL,
    quest_completion_requirement   VARCHAR2(4000) NOT NULL
);

ALTER TABLE questcompletion
    ADD CONSTRAINT questcompletion_quest_key_fk FOREIGN KEY ( quest_key )
        REFERENCES quest ( quest_key );

-- Future Enchancement: Update requirements to be SQL based?

--sample data
INSERT INTO questcompletion (
    quest_key,
    quest_completion_requirement
) VALUES (
    'FQ1',
    'Left Hand has Item'
);

INSERT INTO questcompletion (
    quest_key,
    quest_completion_requirement
) VALUES (
    'FQ1',
    'Right Hand has Item'
);

INSERT INTO questcompletion (
    quest_key,
    quest_completion_requirement
) VALUES (
    'FQ1',
    'Body has Item'
);

INSERT INTO questcompletion (
    quest_key,
    quest_completion_requirement
) VALUES (
    'SQ1',
    'Combat Dummy is unconcious'
);

INSERT INTO questcompletion (
    quest_key,
    quest_completion_requirement
) VALUES (
    'TQ1',
    'Interacted with storekeeper in Northstead'
);

INSERT INTO questcompletion (
    quest_key,
    quest_completion_requirement
) VALUES (
    'HQ1',
    'NewKillCounter(Wolf)>3'
);

INSERT INTO questcompletion (
    quest_key,
    quest_completion_requirement
) VALUES (
    'HQ2',
    'NewKillCounter(Goblin)>4'
);