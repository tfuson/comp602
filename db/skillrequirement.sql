--This creates the skillrequirement table, constraints and sample data
--The skillrequirement table allows for skill trees to be built, where you need to have more basic skills master to gain access to advanced skills

DROP TABLE skillrequirement;

CREATE TABLE skillrequirement (
    skill_key          VARCHAR2(4) NOT NULL,
    skilltree_key      VARCHAR2(4) NOT NULL,
    skillrequirement   VARCHAR2(400) NOT NULL,
    CONSTRAINT skillreq_skill_fk FOREIGN KEY ( skill_key,
                                      skilltree_key )
        REFERENCES skill ( skill_key,
                               skilltree_key )
    ENABLE
);

--Future enchancement: Update requirements to support better support SQL queries

INSERT
    INTO skillrequirement (
    skill_key,
    skilltree_key,
    skillrequirement
) VALUES (
    'STRK',
    'OHW',
    'Must have Skill OHW Swing'
);

INSERT
    INTO skillrequirement (
    skill_key,
    skilltree_key,
    skillrequirement
) VALUES (
    'STRK',
    'OHW',
    'Character Level > 2'
);

INSERT
    INTO skillrequirement (
    skill_key,
    skilltree_key,
    skillrequirement
) VALUES (
    'CHRG',
    'OHW',
    'Must have Skill OHW Strike'
);

INSERT
    INTO skillrequirement (
    skill_key,
    skilltree_key,
    skillrequirement
) VALUES (
    'CHRG',
    'OHW',
    'Character Level > 2'
);

INSERT
    INTO skillrequirement (
    skill_key,
    skilltree_key,
    skillrequirement
) VALUES (
    'SWNG',
    'THW',
    'Character Strenth > 12'
);