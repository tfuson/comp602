--This creates the party table, constraints and sample data
--A party is a collection of characters

DROP TABLE party cascade constraints;

CREATE TABLE party (
    party_key    VARCHAR2(4) NOT NULL,
    party_name   VARCHAR2(40) NOT NULL,
    party_desc   VARCHAR2(400),
    CONSTRAINT party_pk PRIMARY KEY ( party_key ) ENABLE
);

--sample data
INSERT INTO party (
    party_key,
    party_name
) VALUES (
    'HERO',
    'Hero Party'
);

INSERT INTO party (
    party_key,
    party_name
) VALUES (
    'LUCK',
    'Lucky Party'
);

INSERT INTO party (
    party_key,
    party_name
) VALUES (
    'BAD',
    'Bad Guys Party'
);