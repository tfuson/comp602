--This creates a view which shows the plain english description of an item with all the subcomponents

CREATE OR REPLACE VIEW complex_item AS
    SELECT
        'The '
        || lower(item_name)
        || ' is composed of a '
        || lower(substr(components, 0, instr(components, ', ', - 1))
                 || ' and a'
                 || substr(components, instr(components, ', ', -1)+1))
        || '.' AS complex_item
FROM
    item_components
WHERE
    components IS NOT NULL;