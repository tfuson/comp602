--This creates the slot table, constraints and sample data
--The slot table limites the number of items that can be worn by a character

DROP TABLE slot CASCADE CONSTRAINTS;

CREATE TABLE slot (
    slot_key    VARCHAR2(4) NOT NULL,
    slot_name   VARCHAR2(400),
    slot_desc   VARCHAR2(4000),
    CONSTRAINT slot_pk PRIMARY KEY ( slot_key ) ENABLE
);

COMMENT ON COLUMN slot.slot_name IS
    'Name of the slot, left hand, body, etc.';

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'LH',
    'Left Hand'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'RH',
    'Right Hand'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'LR',
    'Left Ring'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'RR',
    'Right Ring'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'A',
    'Arms'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'N',
    'Neck'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'B',
    'Body'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'W',
    'Waist'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'L',
    'Legs'
);

INSERT INTO slot (
    slot_key,
    slot_name
) VALUES (
    'BK',
    'Back'
);