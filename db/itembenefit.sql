--Creates the itembenefit table, constraints and loads sample data
--This shows all of the bonuses associated with items.

DROP TABLE itembenefit;

CREATE TABLE itembenefit (
    item_key       VARCHAR2(4) NOT NULL,
    item_benefit   VARCHAR2(400) NOT NULL,
    CONSTRAINT itemben_item_fk FOREIGN KEY ( item_key )
        REFERENCES item ( item_key )
    ENABLE
);

--Update benefits to support SQL
--Future enhancements: Update table to reference keyword table, add a numeric field to support the bonus amount provided.
--Some thought will need to go into this as some keywords are not numeric, but may require text in the event of a skill name.

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'BSWD',
    '+1 DAMAGE_TYPE=CUT'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'ISWD',
    '+2 DAMAGE_TYPE=CUT'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'ASWD',
    '+3 DAMAGE_TYPE=CUT'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'RSWF',
    'RUNIC'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'RSWF',
    '+5 DAMAGE_TYPE=CUT'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'RSWF',
    '+5 DAMAGE_TYPE=FIRE'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'RSWF',
    '+5 ATTACK'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'WRF',
    '+1 DAMAGE_TYPE=FIRE'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'WRH',
    '+1 SPEED'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'WRF',
    '+1 ATTACK'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'BBP',
    'CAPACITY=10'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'BARM',
    '+1 ARMOR'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'BARM',
    '+1 ARMOR_PENALTY'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'BSHD',
    '+1 ARMOR'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'BBOW',
    '+1 DAMAGE_TYPE=PUNC'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'SILV',
    '1 VALUE'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'GOLD',
    '10 VALUE'
);

INSERT INTO itembenefit (
    item_key,
    item_benefit
) VALUES (
    'GEMS',
    '100 VALUE'
);