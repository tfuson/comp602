--complex_item formats output from item_components
--item_components performs a pivot on the output from nested_items
--nested items flattens the entire item_instances table to show parent and child items

select * from item_components;

select * from complex_item;
--9.016 seconds

--Modify nested_items.sql to only contain items with child items
--This removes items from the view which were not nested
--WHERE child.item_key IS NOT NULL;

select * from complex_item;
--0.163 seconds

