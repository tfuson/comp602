--Creates the itemrequirement table, constraints and loads sample data
--Broad classifications of items, needed to support in-game mechanics like shields for blocking, ranged attacks, slot restrictions, etc.

DROP TABLE itemtype;

CREATE TABLE itemtype (
    item_type_key         VARCHAR2(4) NOT NULL,
    item_type_name        VARCHAR2(40) NOT NULL,
    item_type_desc        VARCHAR2(400) NOT NULL,
    item_max_stack_size   NUMBER DEFAULT 1,
    CONSTRAINT itemtype_pk PRIMARY KEY ( item_type_key ) ENABLE
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'MW',
    'Melee Weapon',
    'Short ranged weapon for combat within the reach of the weapon'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'SHLD',
    'Shield',
    'Special item specifically designed to block incoming attacks'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'RW',
    'Ranged Weapon',
    'Long ranged weapons that are difficult to use in close combat'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'WAND',
    'Wands',
    'Long range magical weapon which require a strong mind to operate'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'STAF',
    'Staffs',
    'Short ranged magical weapons'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'ARMR',
    'Armor',
    'Items worn to reduce the incoming damage at the cost of mobility'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'CON',
    'Container',
    'Items which can hold other items'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'RUNE',
    'Rune',
    'Special Items which can enhance the quality of items which accept runes'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'RING',
    'Ring',
    'Powerful artifacts which embue a variety of effects on the user'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'BOOT',
    'Boots',
    'Footware which enables enhanced protection and movement to the user'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'BRAC',
    'Bracers',
    'Armor which can embue the user with enhanced abilities'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'CLK',
    'Cloak',
    'Cloaks offer a large variety of special defences to the users'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'NECK',
    'Necklaces',
    'Powerful magical items which allows the user to perform special actions'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'LEG',
    'Leggings',
    'Worn to allow for greater feats of movement and defence'
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc,
    item_max_stack_size
) VALUES (
    'CURR',
    'Valuable Items',
    'Used in financial transactions',
    99999
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc,
    item_max_stack_size
) VALUES (
    'REAG',
    'Reagents',
    'Magical components used for magical purposes',
    200
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc,
    item_max_stack_size
) VALUES (
    'POTN',
    'Potions',
    'Single use magical items',
    10
);

INSERT INTO itemtype (
    item_type_key,
    item_type_name,
    item_type_desc
) VALUES (
    'QUES',
    'Quest',
    'Special Quest Items for completing quest objectives'
);