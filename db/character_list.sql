--This is a flattened list of characters, including calculations on level, skill points, location, type and race information
--This could be enhanced in the future to calculate more elements like current heath, attack and defense values
CREATE OR REPLACE VIEW CHARACTER_LIST AS
SELECT
    character_key    AS key,
    character_name   AS name,
    character_type   AS type,
    portrait,
    character.race_key,
    xp,
    character_level(xp) AS char_level,
    character_raw_skill_points(character_level(xp)) AS skill_points,
    character.location_key,
    positionx,
    positiony,
    used_actions,
    used_movement,
    lost_health,
    used_endurance,
    lost_resolve,
    used_mana,
    strength,
    constitution,
    agility,
    intelligence,
    willpower,
    perception,
    race_name,
    race_desc,
    race_speed, 
    location_name,
    location_desc,
    location_parent,
    location_image,
    location_terrain,
    location_sizex,
    location_sizey,
    location_positionx,
    location_positiony
FROM
    character    
    JOIN race ON character.race_key = race.race_key
    JOIN location ON character.location_key = location.location_key
    order by character_type desc,
    char_level asc, character_name asc;