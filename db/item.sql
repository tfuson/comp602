--Creates the item table, constraints and loads sample data
--The items table stores all the types of ojects in the game, the total number of items would likely need to be less than 1000.
--See iteminstance for specific objects, as there could be more that 1 beginner sword
--See itemtype for broad classifications of items
--See itembenefit and itemrequirement for the bonuses and restrictions associated with the items defined in this table
DROP TABLE item;

CREATE TABLE item (
    item_key        VARCHAR2(4) NOT NULL,
    item_name       VARCHAR2(40) NOT NULL,
    item_desc       VARCHAR2(400),
    item_type_key   VARCHAR2(4) NOT NULL,
    item_picture    BLOB,
    CONSTRAINT item_pk PRIMARY KEY ( item_key ),
    CONSTRAINT item_type_key_fk FOREIGN KEY ( item_type_key )
        REFERENCES itemtype ( item_type_key )
    ENABLE
);

--Sample data    
INSERT INTO item (
        item_key,
        item_name,
        item_type_key)
VALUES ('BSWD','Beginner Sword','MW');

INSERT INTO item (
        item_key,
        item_name,
        item_type_key)
VALUES ('ISWD','Intermediate Sword','MW');

INSERT INTO item (
        item_key,
        item_name,
        item_type_key)
VALUES ('ASWD','Advanced Sword','MW'
);

INSERT INTO item (
    item_key,
        item_name,
    item_type_key
) VALUES (
    'RSWF',
    'Runic Sword of Fire',
    'MW'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'SILV',
    'A Stack of Silver Coins',
    'CURR'
);

INSERT INTO item (
        item_key,
    item_name,
    item_type_key
) VALUES (
    'GOLD',
    'A Stack of Gold Coins',
    'CURR'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'GEMS',
    'Valuable Gems',
    'CURR'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'WRF',
    'Weak Rune of Fire',
    'RUNE'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'WRH',
    'Weak Rune of Haste',
    'RUNE'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'WRL',
    'Weak Rune of Light',
    'RUNE'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'BBP',
    'Beginner Backpack',
    'CON'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'BSHD',
    'Beginner Shield',
    'SHLD'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'BBOW',
    'Beginner Bow',
    'RW'
);

INSERT INTO item (
    item_key,
    item_name,
    item_type_key
) VALUES (
    'BARM',
    'Beginner Armor',
    'ARMR'
);