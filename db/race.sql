DROP TABLE race CASCADE CONSTRAINTS;

CREATE TABLE race (
    race_key                  VARCHAR2(4) NOT NULL,
    race_name                 VARCHAR2(40) NOT NULL,
    race_desc                 VARCHAR2(400),
    race_speed                NUMBER DEFAULT 5,
    CONSTRAINT race_pk PRIMARY KEY ( race_key ) ENABLE
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'HUMN',
    'Human',
    5
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'ELF',
    'Elf',
    5
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'DWRF',
    'Dwarf',
    4
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'ORC',
    'Orc',
    5
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'HLFL',
    'Halfling',
    5
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'FRSP',
    'Fire Sprit',
    6
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'CELS',
    'Celestial',
    5
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'DEMN',
    'Demon',
    5
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'OGRE',
    'Ogre',
    5
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'GOBL',
    'Goblin',
    4
);

INSERT INTO race (
    race_key,
    race_name,
    race_speed
) VALUES (
    'GNM',
    'Gnome',
    4
);