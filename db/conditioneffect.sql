--This creates the conditioneffects table, constraints and sample data
--Each condition may have multiple in-game effects

DROP TABLE conditioneffects;

CREATE TABLE conditioneffects (
    condition_key      VARCHAR2(4) NOT NULL,
    condition_effect   VARCHAR2(200) NOT NULL,
    CONSTRAINT condeff_condition_key_fk FOREIGN KEY ( condition_key )
        REFERENCES condition ( condition_key )
    ENABLE
);

--Future Enhancement: Update requirements to support SQL

--Load Sample Data
INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'FLEE',
    'Move at full speed to the nearest map edge'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'DEAD',
    'Character is not playable'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'U',
    'Actions=0'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'U',
    'Speed=0'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'PT',
    'Actions=0'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'PT',
    'Speed=0'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'PT',
    'Add resistance FIRE=100'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'PT',
    'Add resistance SLSH=100'
);

INSERT INTO conditioneffects (
    condition_key,
    condition_effect
) VALUES (
    'ROOT',
    'Speed=0'
);