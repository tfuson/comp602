--Sample queries which show the proper operation of views, functions and tables.
--Demonstrates learning objectives

SELECT * FROM characterquest;
SELECT * FROM quest;

--Complex Queries as views
SELECT * FROM character_list;
SELECT * FROM party_info;
SELECT * FROM item_instances;
SELECT * FROM item_components;
SELECT * FROM nested_items;
SELECT * FROM complex_item;

--Write the same join as a sub-query-------------------------
--Shows quests that have the status of active
SELECT
    quest.quest_name
FROM
    quest
    JOIN characterquest ON quest.quest_key = characterquest.quest_key
WHERE
    quest_status = 'ACTIVE';

--Shows quests that have the status of active, same as above
SELECT
    quest.quest_name
FROM
    quest
WHERE
    quest_key IN (
        SELECT
            quest_key
        FROM
            characterquest
        where
            quest_status = 'ACTIVE'
    );
---------------------------------------------

UPDATE character
SET xp=200
WHERE character_key='BIGB';
COMMIT;

