--This creates the partycharacters table, constraints and sample data
--This table stores what characters are in what party
DROP TABLE partycharacters;

CREATE TABLE partycharacters (
    party_key       VARCHAR2(4) NOT NULL,
    character_key   VARCHAR2(4) NOT NULL,
    CONSTRAINT partchar_party_key_fk FOREIGN KEY(party_key)
        REFERENCES party(party_key),
    CONSTRAINT partchar_character_key_fk FOREIGN KEY ( character_key )
        REFERENCES character ( character_key )
    ENABLE
);

--sample data
INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'HERO',
    'BOBF'
);

INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'HERO',
    'ROGR'
);

INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'HERO',
    'NANC'
);

INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'HERO',
    'KIMA'
);

INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'LUCK',
    'UNLR'
);

INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'LUCK',
    'UNLW'
);

INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'LUCK',
    'UNLC'
);

INSERT INTO partycharacters (
    party_key,
    character_key
) VALUES (
    'BAD',
    'BIGB'
);