--Creates the raceresistance table, constraints and loads sample data
--This associates certain protections to a character depending on the race they have selected for thier character.
DROP TABLE raceresistance;

CREATE TABLE raceresistance (
    race_key          VARCHAR2(4) NOT NULL,
    damage_type_key   VARCHAR2(4) NOT NULL,
    resistance        NUMBER,
    CONSTRAINT raceresistance_pk PRIMARY KEY ( race_key,
                                               damage_type_key ),
    CONSTRAINT raceresistance_race_key_fk FOREIGN KEY ( race_key )
        REFERENCES race ( race_key ),
    CONSTRAINT raceresistance_damage_type_fk FOREIGN KEY ( damage_type_key )
        REFERENCES damagetype ( damage_type_key )
    ENABLE
);

--Conversion of damage_type to keyword table
ALTER TABLE raceresistance DROP CONSTRAINT raceresistance_damage_type_fk;

ALTER TABLE raceresistance
    ADD CONSTRAINT raceresistance_keyword_fk FOREIGN KEY ( damage_type_key )
        REFERENCES keyword ( keyword_key );

COMMENT on COLUMN raceresistance.resistance is
    'Damage resistance bonus in percentage';

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'FRSP',
    'FIRE',
    100
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'FRSP',
    'WATR',
    -50
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'OGRE',
    'CUT',
    50
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'OGRE',
    'PUNC',
    50
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'OGRE',
    'CRSH',
    30
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'DEMN',
    'FIRE',
    50
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'GOBL',
    'FIRE',
    20
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'GOBL',
    'POIS',
    80
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'ORC',
    'CUT',
    20
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'ORC',
    'CRSH',
    10
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'ORC',
    'PUNC',
    20
);

INSERT INTO raceresistance (
    race_key,
    damage_type_key,
    resistance
) VALUES (
    'ORC',
    'POIS',
    50
);